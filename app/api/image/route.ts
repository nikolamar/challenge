import type { NextApiRequest, NextApiResponse } from 'next';
import { NextResponse } from 'next/server';
import { OPENAI_API_KEY } from '@/constants/constants';

export async function POST(req: any) {
  const { prompt } = await req.json();
  console.log('server prompt:', prompt);

  if (!prompt) {
    return new NextResponse(
      JSON.stringify({ name: "Please provide something for ChatGPT" }),
      { status: 400 }
    );
  }

  try {
    const response = await fetch('https://api.openai.com/v1/images/generations', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        model: "dall-e-3",
        prompt,
        n: 1,
        size: "1024x1024"
      }),
    });

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }

    const data = await response.json();

    return new NextResponse(
      JSON.stringify({ image: data }),
      { status: 200 }
    );
  } catch (error) {
    console.error('OpenAI API request failed:', error);
    return new NextResponse(
      JSON.stringify({ message: 'Server error' }),
      { status: 500 }
    );
  }
}
