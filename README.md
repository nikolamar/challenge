# Challenge

1. Create env.local with secret key
```
OPENAI_API_KEY=yoursecrethere
```

2. To test text generation type in prompt something like

```
generate short story
```

3. To test image generation in prompt type first something like

```
generate image of snowman
```

It is important for you input text to contain image so it can generate...
