export async function sendMessageToChatGPT(prompt: string) {
  try {
    const response = await fetch('http://localhost:3000/api/chat', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ prompt }),
    });

    if (!response.ok) {
      throw new Error(`Network response was not ok, status: ${response.status}`);
    }

    const data = await response.json();
    const chatResponse = data.message.choices[0].text;
    console.log(chatResponse);
    return chatResponse;
  } catch (error) {
    console.error('Failed to send message:', error);
    // Handle the error appropriately in your UI
    return null;
  }
}
