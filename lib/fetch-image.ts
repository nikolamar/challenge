export async function sendMessageToDALLE(prompt: string) {
  try {
    const response = await fetch('http://localhost:3000/api/image', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ prompt }),
    });

    if (!response.ok) {
      throw new Error(`Network response was not ok, status: ${response.status}`);
    }

    const data = await response.json();
    const dalleResponse = data.image.data[0].url;
    return dalleResponse;
  } catch (error) {
    console.error('Failed to generate image:', error);
    // Handle the error appropriately in your UI
    return null;
  }
}
