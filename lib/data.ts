export const userData = [
  {
    id: 1,
    avatar: '/ChatGPT.png',
    messages: [
      {
        id: 1,
        avatar: '/User1.png',
        name: 'Chat GPT',
        message: 'Hello World!',
      },
    ],
    name: 'Chat GPT',
  },
];

export type UserData = (typeof userData)[number];

export const loggedInUserData = {
  id: 5,
  avatar: '/LoggedInUser.jpg',
  name: 'Jakob Hoeg',
};

export type LoggedInUserData = (typeof loggedInUserData);

export interface Message {
  id: number;
  avatar: string;
  name: string;
  message: string;
}

export interface User {
  id: number;
  avatar: string;
  messages: Message[];
  name: string;
}