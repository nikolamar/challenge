import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export function deepCopy<T>(obj: T): T {
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  if (Array.isArray(obj)) {
    const arrCopy = obj.map((val) => deepCopy(val));
    return arrCopy as T;
  }

  if (obj instanceof Object) {
    const objCopy = Object.keys(obj).reduce((copy, key) => {
      (copy as any)[key] = deepCopy((obj as any)[key]);
      return copy;
    }, {});
    return objCopy as T;
  }

  throw new Error('Unable to copy obj! Its type isn\'t supported.');
}
