import { sendMessageToChatGPT } from '@/lib/fetch-chatgpt';
import { sendMessageToDALLE } from '@/lib/fetch-image';
import { create } from 'zustand';

export interface Message {
  id: number;
  name: string;
  avatar: string;
  message?: string;
  image?: string;
}

interface ChatMessage {
  messages: Message[];
  sendMessage: (message: Message) => void;
  clearMessages: () => void;
  sendToChatGPT: (userMessage: string) => void; // Add this line
}

// Define a store using 'create'
const useChatStore = create<ChatMessage>((set, get) => ({
  messages: [], // Initial state of messages

  // Action to add a new message
  sendMessage: (message: Message) =>
    set(state => ({ messages: [...state.messages, message] })),

  // Action to clear all messages
  clearMessages: () => {
    set({ messages: [] })
  },

  // Add this new action to handle sending messages to ChatGPT
  sendToChatGPT: async (userMessage: string) => {
    console.log('store userMessage', userMessage);
    const containsImage = userMessage.toLowerCase().includes("image".toLowerCase()); // true
    if (containsImage) {
      const dalleImage = await sendMessageToDALLE(userMessage);
      console.log(dalleImage);
      const messages = get().messages;
      const newMessage: Message = {
        id: messages.length + 1,
        name: 'Nikola',
        avatar: "http://localhost:3000/ChatGPT.png",
        image: dalleImage,
      };
      set(state => ({ messages: [...state.messages, newMessage] }));
    } else {
      const chatGPTMessage = await sendMessageToChatGPT(userMessage);
      console.log(chatGPTMessage);
      const messages = get().messages;
      const newMessage: Message = {
        id: messages.length + 1,
        name: 'Nikola',
        avatar: "http://localhost:3000/ChatGPT.png",
        message: chatGPTMessage,
      };
      set(state => ({ messages: [...state.messages, newMessage] }));
    }
  },
}));

export default useChatStore;
