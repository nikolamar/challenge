// In custom.d.ts or declarations.d.ts
declare module "*.wgsl" {
  const content: string;
  export default content;
}
