import { Message, UserData } from "@/lib/data";
import { cn } from "@/lib/utils";
import React, { useEffect, useRef } from "react";
import { Avatar, AvatarImage } from "@/components/ui/avatar";
import ChatBottombar from "./chat-bottombar";
import { AnimatePresence, motion } from "framer-motion";
import useChatStore from "@/store/store";
import Image from "next/image"; // Import Next.js Image component for optimized images

interface ChatListProps {
  selectedUser: UserData;
  isMobile: boolean;
}

export function ChatList({
  selectedUser,
  isMobile
}: ChatListProps) {
  const messagesContainerRef = useRef<HTMLDivElement>(null);
  const messages = useChatStore((state) => state.messages);

  useEffect(() => {
    if (messagesContainerRef.current) {
      const { current } = messagesContainerRef;
      current.scrollTop = current.scrollHeight;
    }
  }, [messages]);

  return (
    <div className="w-full overflow-y-auto overflow-x-hidden h-full flex flex-col">
      <div
        ref={messagesContainerRef}
        className="w-full overflow-y-auto overflow-x-hidden h-full flex flex-col"
      >
        <AnimatePresence>
          {messages.map((message, index) => (
            <motion.div
              key={index}
              layout
              initial={{ opacity: 0, scale: 1, y: 50, x: 0 }}
              animate={{ opacity: 1, scale: 1, y: 0, x: 0 }}
              exit={{ opacity: 0, scale: 1, y: 1, x: 0 }}
              transition={{
                opacity: { duration: 0.1 },
                layout: {
                  type: "spring",
                  bounce: 0.3,
                  duration: messages.indexOf(message) * 0.05 + 0.2,
                },
              }}
              style={{
                originX: 0.5,
                originY: 0.5,
              }}
              className={cn(
                "flex flex-col gap-2 p-4 whitespace-pre-wrap",
                message.name !== selectedUser.name ? "items-end" : "items-start"
              )}
            >
              <div className="flex gap-3 items-center">
                <Avatar className="flex justify-center items-center">
                  <AvatarImage
                    src={message.avatar}
                    alt={message.name}
                    width={6}
                    height={6}
                  />
                </Avatar>
                {message.message ? (
                  <span className="bg-accent p-3 rounded-md max-w-xs">
                    {message.message}
                  </span>
                ) : null}
                {message.image ? (
                  <div className="max-w-xs rounded-md overflow-hidden">
                    <Image src={message.image} alt="Chat Image" width={200} height={200} />
                  </div>
                ) : null}
              </div>
            </motion.div>
          ))}
        </AnimatePresence>
      </div>
      <ChatBottombar isMobile={isMobile} />
    </div>
  );
}
